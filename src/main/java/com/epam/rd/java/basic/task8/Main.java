package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document =  builder.parse(xmlFileName);

		// get container
		Element element = document.getDocumentElement();



		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
	}

}
